﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sort_test
{
    class Program
    {
        static void Main(string[] args)
        {
            // kui keerukas on ja kui kaua kestab random arvude järjestamine
            int mitu = 1000; // määrad väärtuse , mida programm kasutab, on lihtsam muuta vajadusel
            Random r = new Random();

            //int[] arvud = new int[mitu];
            List<int> arvud = new List<int>(mitu);

            for (int i = 0; i < mitu; i++)
            {
                // arvud[i] = r.Next();
                arvud.Add(r.Next());
            }
            //  foreach (var x in arvud) Console.WriteLine($"{x} ");

            SortedSet<int> sorditud = new SortedSet<int>();
            DateTime algus = DateTime.Now; // ***märgin alguse

            //1. teeme listi arrayks ja sorteerime
            // Array.Sort(arvud);

            //2. sorteerimie listi enda ja tulemuse teeme listiks tagasi
            // arvud = arvud.OrderBy(x => x).ToArray(); // sorteerib ära ja teeb sellest massiivi
            // arvud = arvud.OrderBy(x => x).ToList(); // kui on Listiga tehtud, tuleb panna ToList

            //3. pistame sorteeritud listi 
            foreach (var x in arvud) sorditud.Add(x);
        
            Console.WriteLine((DateTime.Now - algus).TotalMilliseconds); //  *** mõõdan kestvuse
        }
    }
}
